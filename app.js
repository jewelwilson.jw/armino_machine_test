const express = require('express')
const mongoose = require('mongoose')
const bodyParser = require('body-parser')

const dotenv = require("dotenv")
dotenv.config();
const multer = require("multer");
multer({ dest: "uploads/" });

mongoose.connect("mongodb://localhost:27017/demo_armino",{
    useNewUrlParser:true,
    useCreateIndex:true,
    useUnifiedTopology:true,
})

const db = mongoose.connection;
db.on("error",(error)=>console.log(error));
db.once("open", ()=> console.log("connection success"));

const app = express();

app.use(bodyParser.json());
app.use("/uploads", express.static("uploads"));

const apiRouter = require("./src/routes/api");
app.use("/api/v1",apiRouter);

app.listen("4000",()=>console.log("App Running"));