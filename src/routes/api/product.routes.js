const express = require('express');
const router = express.Router();
const middleware = require('../../middlewares/product.middleware')

const productController = require('../../controllers/product.controller');

// get all data
router.get('/all', productController.list);
// get data using id
router.get('/:id', productController.show);
// post data
router.post('/create',(req,res,next)=>middleware.create(req,res,next),(req,res)=>productController.create(req,res));
// update data using id
router.put('/update/:id',(req,res,next)=>middleware.update(req,res,next),(req,res)=>productController.update(req,res));
// delete data using id
router.delete('/delete/:id',productController.delete);


module.exports = router;