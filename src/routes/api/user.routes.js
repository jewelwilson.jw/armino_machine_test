const express = require('express');
const router = express.Router();
// const verify = require('../../middlewares/verifyToken');

const userController = require('../../controllers/user.controller');

router.post('/create', userController.create);
router.get('/all',userController.list);
router.get('/show/:id',userController.show);
router.put('/update/:id',userController.update);
router.delete('/delete/:id', userController.delete);
router.post('/login',userController.login);

module.exports = router;