const express = require("express");

const router = express.Router();

const userRouter = require("./user.routes");
const productRouter = require("./product.routes")
const promocodeRouter = require("./promocode.routes");
const cartRouter = require("./cart.routes")

router.use("/users",userRouter)
router.use("/products",productRouter)
router.use("/promocode",promocodeRouter)
router.use("/cart",cartRouter)

module.exports = router;