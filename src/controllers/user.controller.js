const userService = require("../services/userService");
const {createValidation} = require("../validation/schema/user.schema");
const { updateValidation } = require("../validation/schema/user.schema");

exports.list = async function (req, res, next) {
    try {
        const user = await userService.list(res);
        return res.json({ status: "success", data: user });
    } catch (e) {
        return res.json({ status: "error", message: e.message });
    }
};
// api for getting data using id

exports.show = async function (req, res, next) {
    const id = req.params.id;

    try {
        const user = await userService.show(id, res);
        return res.json({ status: "success", data: user });
    } catch (e) {
        return res.json({ status: "error", message: e.message });
    }
};

// api for posting
exports.create = async function (req, res, next) {
    const user = req.body;

    try {
        const { error } = await createValidation(user);

        if (error) return res.json({ status: "error", message: error.message })
        dat = await userService.create(user, res);
        return res.json({ status: "success", data: dat });
    } catch (e) {
        return res.json({ status: "error", message: e.message });
    }
};

// api for update
exports.update = async function (req, res, next) {
    try {
        const id = req.params.id;
        const user= req.body;
        const { error } = await updateValidation(user);

        if (error) return res.json({ status: "error", message: error.message })
        data = await userService.update(id, user, res);
        return res.json({ status: "success", data: data });
    } catch (e) {
        return res.json({ status: "error", message: e.message });
    }
};
// api for delete
exports.delete = async function (req, res, next) {
    const id = req.params.id;

    try {
        const dat = await userService.delete(id, res);

        return res.json({ status: "success", data: dat });
    } catch (e) {
        return res.json({ status: "error", message: e.message });
    }
};

exports.login = async function(req,res){
    try{
        const user = req.body
        const dat = await userService.login(user,res)
        return res.json({data:dat})
    }
    catch(e){
        return res.json({status:"error",msg:e.message})
    }
}
