const productService = require("../services/productService");
const { productValidation } = require("../validation/schema/product.schema");
const { updateValidation } = require("../validation/schema/product.schema");
exports.list = async function (req, res, next) {
  try {
    const { limit } = req.query;
    const { page } = req.query;
    const { filter } = req.query;
    const { sort } = req.query;
    const product = await productService.list(
      parseInt(page),
      parseInt(limit),
      filter,
      sort
    );
    // const product = await productService.list({}, (page = 10), (limit = 10));
    return res.status(200).json(product);
  } catch (e) {
    return res.status(400).json({ status: "error", message: e.message });
  }
};
// api for getting data using id

exports.show = async function (req, res, next) {
  const id = req.params.id;

  try {
    const product = await productService.show(id);
    return res.status(200).json({ status: "success", data: product });
  } catch (e) {
    return res.status(400).json({ status: "error", message: e.message });
  }
};

// api for posting
exports.create = async function (req, res, next) {
  // Lets Validate the data before a user register
  const product = req.body
  try {
    const { error } = await productValidation(product);
    if (error) return res.status(422).send(error.details);
    dat = await productService.create(product);
    return res.status(200).json({ status: "success", data: dat });
  } catch (e) {
    return res.status(400).json({ status: "error", message: e.message });
  }
};

// api for update
exports.update = async function (req, res, next) {
  try {
    const id = req.params.id;
    const product = req.body;
    const { error } = await updateValidation(product);
    if (error) return res.status(422).send(error.details);
    data = await productService.update(id, product);
    return res.status(200).json({ status: "success", data: data });
  } catch (e) {
    return res.status(400).json({ status: "error", message: e.message });
  }
};
// api for delete
exports.delete = async function (req, res, next) {
  const id = req.params.id;

  try {
    const dat = await productService.delete(id);

    return res.status(200).json({ status: "success", data: dat });
  } catch (e) {
    return res.status(400).json({ status: "error", message: e.message });
  }
};