const Joi = require('@hapi/joi');
const createValidation = data => {
    const schema = Joi.object({
        username: Joi.string().min(4).optional(),
        email:Joi.string().optional(),
        phone:Joi.number().optional(),
        password:Joi.string().optional(),
    });
    return schema.validate(data);
};
const updateValidation = data => {
    const schema = Joi.object({
        username: Joi.string().min(4).optional(),
        email:Joi.string().optional(),
        phone:Joi.number().optional(),
        password:Joi.string().optional(),
    });
    return schema.validate(data);
};
module.exports.createValidation = createValidation;
module.exports.updateValidation = updateValidation;
