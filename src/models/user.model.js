const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let studentSchema = new Schema({
    username: { type: String, required: false },
    email: { type: String, required: false },
    phone: { type: String, required: false },
    password: { type: String, required: false },
},
    {
        collection: 'user'
    })
module.exports = mongoose.model('User', studentSchema)
