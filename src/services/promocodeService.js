const  Promocode  = require("../models/promocode.model");
 

// service for get all  data
exports.list = async function (res) {
    try {
        const promo = await Promocode.find({});
        return promo;
    } catch (e) {
        return res.json({ status: "error", message: e.message });
    }
};


// service for get data using id
exports.show = async function (id, res) {
    try {
        const promo = await Promocode.findById(id);
        return promo;
    } catch (e) {
        return res.json({ status: "error", message: e.message });
    }
};
// service for post data
exports.create = async function (promo, res) {
    try {
        const dat = await new Promocode(promo);
        await dat.save();
        return dat;
    } catch (error) {
        return res.json({ status: "error", message: error.message });
    }
};
// service for update data using id
exports.update = async function (id, promo, res) {
    try {
        const dat = await Promocode.findByIdAndUpdate(id, promo, { new: true });
        return dat;
    } catch (error) {
        return res.json({ status: "error", message: error.message });
    }
};
// service for delete data using id
exports.delete = async function (id, res) {
    try {
        const dat = await Promocode.findByIdAndRemove(id);
        return dat;
    } catch (error) {
        return res.json({ status: "error", message: error.message });
    }
};