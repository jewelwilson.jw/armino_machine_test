const Product = require("../models/product.model");
const pagination = require("../middlewares/datatools");

// service for get all  data
exports.list = async function (page, limit, filter, sort) {
  try {
    const product = await pagination.paginate(page,limit,filter,sort,Product);
    // const  product = await   Product.find({});
    return product;
  } catch (e) {
    throw Error("Error while getting all data  ");
  }
};
// service for get data using id
exports.show = async function (id) {
  try {
    const product = await Product.findById(id);
    return product;
  } catch (e) {
    throw Error("Error while finding data with ID ");
  }
};
// service for post data
exports.create = async function (product) {
  try {
    const dat = await new Product(product);
    await dat.save();
    return dat;
  } catch (error) {
    throw Error("Error while Posting data ");
  }
};
// service for update data using id
exports.update = async function (id, product) {
  try {
    const dat = await Product.findByIdAndUpdate(id, product, { new: true });
    return dat;
  } catch (error) {
    throw Error("Error while updating data ");
  }
};
// service for delete data using id
exports.delete = async function (id) {
  try {
//delete image files from server
    const kill = await Product.findById(id);
    if (kill.featuredImage){
    oldpath = kill.featuredImage
    path = oldpath.replace(`${process.env.HOST_NAME}/`,'');
    if (path)
   await pagination.deleteimage(path),(err)=>errorHandler(res,err)
    }
// delete database details
    const dat = await Product.findByIdAndRemove(id);
    return dat;
  } catch (error) {
    throw Error("Error while deleting data");
  }
};