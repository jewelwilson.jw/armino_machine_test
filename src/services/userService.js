const  User  = require("../models/user.model");
const jwt = require('jsonwebtoken');
const secret = process.env.TOKEN_SECRET
 

// service for get all  data
exports.list = async function (res) {
    try {
        const user = await User.find({});
        return user;
    } catch (e) {
        return res.json({ status: "error", message: e.message });
    }
};


// service for get data using id
exports.show = async function (id, res) {
    try {
        const user = await User.findById(id);
        return user;
    } catch (e) {
        return res.json({ status: "error", message: e.message });
    }
};
// service for post data
exports.create = async function (user, res) {
    try {
        const dat = await new User(user);
        await dat.save();
        return dat;
    } catch (error) {
        return res.json({ status: "error", message: error.message });
    }
};
// service for update data using id
exports.update = async function (id, user, res) {
    try {
        const dat = await User.findByIdAndUpdate(id, user, { new: true });
        return dat;
    } catch (error) {
        return res.json({ status: "error", message: error.message });
    }
};
// service for delete data using id
exports.delete = async function (id, res) {
    try {
        const dat = await User.findByIdAndRemove(id);
        return dat;
    } catch (error) {
        return res.json({ status: "error", message: error.message });
    }
};

exports.login =async function(user,res){
    try{
        phno = user.phone;
        const check = await User.findOne({ phone: phno });
        password = check.password;
                 if (user.password !== check.password)
         throw Error('invalid password');
         const payload = { id: check.id, username: check.username };
         const token = jwt.sign(payload, secret, { expiresIn: 36000 });
                  
           return {
            _id: check._id,
            username: check.username,
            user_authentication: "sucess",
            token,
          };
        } catch (e) {
          return res.json({ status: "error", message: e.message });
        
    }
}


